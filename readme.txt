- add 2 products LEMONS and TOMATOS to the CART and display all the items from the cart and total SUM: 
     
    8 Lemons: 4 EUR ..... 0.5 EUR/Piece
    25 Tomatos: 4.5 EUR ..... 0.18 EUR/Piece

- apply discount for each product

  	1-9 items  Lemons  unit price  0.50 Euro
  	10+ items  Lemons  unit price  0.45 Euro

  	1-19  items  Tomatos  unit price   0.20 Euro
  	20-99 items  Tomatos  unit price   0.18 Euro
  	99+   items  Tomatos  unit price   0.14 Euro

- use Interface for Cart and add the methods below:

    function getTotalSum();
    function addItem(Product $product, $amount);
    function getPriceOf(Product $product);