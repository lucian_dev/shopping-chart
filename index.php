<?php
spl_autoload_register(function ($class) {
    include 'classes/' . $class . '.php';
});


//create and set productS default
$productLemon = new Product();
$productLemon->name = "Lemons";
$productLemon->price = 0.50;
$productLemon->quantity = 1;

$productTomatoes = new Product();
$productTomatoes->name = "Tomatos";
$productTomatoes->price = 0.50;
$productTomatoes->quantity = 1;

//create and set basket
$myCart = new Cart();
$myCart->addItem($productLemon, 8);
$myCart->addItem($productTomatoes, 25);

/**
* VIEW
*/
$html = new ViewShop($myCart);
echo $html->render();