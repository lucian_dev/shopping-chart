<?php
/**
 *  Interface zone
 * 
 * @package classes
 * @author Lucian C.
 */
interface ICart {
    public function getTotalSum();
    public function addItem(Product $product, $amount);
    public function getPriceOf(Product $product);
}

interface IDiscount {
    public function getDiscount(Product $product);
}