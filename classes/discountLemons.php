<?php
require_once('interface.php');
/**
 *  Class DiscountLemons
 * 
 *  getDiscount()  - apply discount
 * 
 * @package classes
 * @author Lucian C.
 */
class DiscountLemons implements IDiscount{

    public function __construct(){}
    public function getDiscount(Product $product){
        $amount = $product->quantity;

        if (1 <= $amount && $amount <= 9) {
            $product->price = 0.50;
        }
        
        elseif ($amount >= 10) {
            $product->price = 0.45;
        }
        
        return $product;
    }
}//end class