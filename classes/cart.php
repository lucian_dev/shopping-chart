<?php
require_once('interface.php');
/**
 *  Class Cart 
 * 
 *  getTotalSum()  - get total sum of cart
 *  getItems()     - list with items from the cart
 *  addItem()      - add item to the shooping cart
 *  getPriceOf()   - show price of the product
 *  applyDiscount()  - apply the discount 
 * 
 * 
 * @package classes
 * @author Lucian C.
 */
class Cart implements ICart{

    private $list = array();
    private $currProd ='';

    public function __construct(){}

    public function getTotalSum() {
        $list = $this->list;
        $sum = 0;
        foreach ($list as $key => $obj) {
          $sum += $obj->price * $obj->quantity;
        }// end foreach
        return $sum;
    }

    public function getItems(){
        return $this->list;
    }
    
    /**
     * Description: add item to the shooping car
     * @param type Product $product 
     * @param type int     $amount 
     * @return none
     */
    public function addItem(Product $product, $amount){
        //need the item id
        
        $product->quantity = $amount;
        $this->currProd = $product;

        //check discount
        $this->applyDiscount();
        $this->list[] = $this->currProd;
    }

    //get the price of the single product
    public function getPriceOf(Product $product){
        return $product->price;
    }

    private function applyDiscount(){

        //check what type of discount can be apply
        if (strtolower($this->currProd->name) == 'lemons') {
            $discount = new DiscountLemons();
        }

        if (strtolower($this->currProd->name) == 'tomatos') {
            $discount = new DiscountTomatos();
        }
        //if the product is other type the discount is default
        if(isset($discount)){
            $this->currProd = $discount->getDiscount($this->currProd);    
        }
    }
}//end class