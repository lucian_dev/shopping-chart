<?php
/**
 *  Class Product 
 * 
 *  Attributes name, price and quantity
 * 
 * @package classes
 * @author Lucian C.
 */
class Product {  
    private $name = '';
    private $price = '';
    private $quantity = '';

    public function __construct() {}

    //Set getter and setter
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }
}//end class