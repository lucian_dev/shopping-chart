<?php
/**
 * View shopping cart 
 * 
 * @package MyProject
 * @author Lucian C.
 */
class ViewShop {

    private $chart = '';
    private $output = '';

    public function  __construct(Cart $list){
        $this->chart = $list;
    }

    /**
     * Ouput the list with products and show TOTAL price
     * @return string 
    */
    public function render(){
        $myCart = $this->chart;
        $output = $this->output;
        $list = $myCart->getItems();

        $output .= "<pre>";
        $output .= "<h1>CART</h1>";

        foreach ($list as $key => $obj) {
          $output .= $obj->quantity.' '. $obj->name .": ". $obj->price*$obj->quantity . " EUR ..... " . $obj->price. " EUR/Piece";
          $output .= "<br>";
        }

        $output .= "<br>";
        $output .= "TOTAL";
        $output .= "<br>";
        $output .= $myCart->getTotalSum() . ' EUR';
        $output .= "</pre>";
         
        return $output;
    }

}