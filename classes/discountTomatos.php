<?php
require_once('interface.php');
/**
 *  Class DiscountTomatos
 * 
 *  getDiscount()  - apply discount
 * 
 * @package classes
 * @author Lucian C.
 */
class DiscountTomatos implements IDiscount{
    
    public function __construct(){}
    public function getDiscount(Product $product){
        $amount = $product->quantity;

        if (1 <= $amount && $amount <= 19) {
            $product->price = 0.20;
        }
        
        elseif (20 <= $amount && $amount <= 99) {
            $product->price = 0.18;
        }
        
        elseif ($amount > 99) {
            $product->price = 0.14;
        }

        return $product;
    }
}//end class